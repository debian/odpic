Source: odpic
Priority: optional
Maintainer: Joseph Nahmias <jello@debian.org>
Build-Depends:
 debhelper-compat (= 13),
Build-Depends-Indep:
 dh-sequence-sphinxdoc,
 python3-sphinx <!nodoc>,
Standards-Version: 4.6.1
Section: contrib/libs
Homepage: https://oracle.github.io/odpi/
Vcs-Browser: https://salsa.debian.org/debian/odpic
Vcs-Git: https://salsa.debian.org/debian/odpic.git
Rules-Requires-Root: no
Description: Oracle Database Programming Interface
 Oracle Database Programming Interface for C (ODPI-C) is an open source
 library of C code that simplifies access to Oracle Database for applications
 written in C or C++. It is a wrapper over Oracle Call Interface (OCI) that
 makes applications and language interfaces easier to develop.
 .
 ODPI-C supports basic and advanced features of Oracle Database and Oracle
 Client. See the homepage for a list.

Package: odpic-dev
Section: contrib/libdevel
Architecture: any
Multi-Arch: same
Depends: libodpic4 (= ${binary:Version}), ${misc:Depends}
Recommends: odpic-doc
Description: ${source:Synopsis} - development files
 ${source:Extended-Description}
 .
 This package contains the files needed to develop programs that use ODPI-C;
 including the headers, examples, and sample programs.

Package: libodpic4
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: libaio1
Description: ${source:Synopsis} - runtime C shared library
 ${source:Extended-Description}
 .
 This package contains the C shared library needed to run programs that
 dynamically link to ODPI-C.

Package: odpic-doc
Section: contrib/doc
Architecture: all
Multi-Arch: foreign
Depends: ${sphinxdoc:Depends}, ${misc:Depends}
Description: ${source:Synopsis} - documentation
 ${source:Extended-Description}
 .
 This package contains the developer documentation for ODPI-C.
